# zotero

# zotero discontinued. Instead use zotero-bin

Zotero Standalone. Is a free, easy-to-use tool to help you collect, organize, cite, and share your research sources.

http://www.zotero.org/download

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/zotero.git
```

